## A parametric point cloud surface reconstruction algorithm

A repository for a MIPRO 2022 conference article.

# Abstract

One of the most general ways to represent a three-dimensional domain is to give its boundary in the form of a dense point cloud. This is what we are given when using a 3D scanner, for example. However, such a point cloud on its own is often insufficient for further calculations. On one hand, it may be uneven or contain gaps. On the other hand, there is no natural way of determining if a given point is in the interior of the domain, represented by the point cloud. In this paper we propose an algorithm that fits parametrized surfaces to discrete neighbourhoods that cover the point cloud, then uses a partition of unity to ensure the surfaces match along their edges. We then use this local parametrization to construct the characteristic function of the domain. The algorithm enables enables us to work with more types of surfaces at once, as many representations of surfaces (e.g. those using NURBS, triangle meshes) can be reduced to point clouds.
