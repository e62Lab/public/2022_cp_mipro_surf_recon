\documentclass{MIPRO}

\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage[T1]{fontenc}
\usepackage{flushend}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{array}
\usepackage{url}

\newcommand{\R}{\mathbb{R}}
\renewcommand{\vec}{\boldsymbol}
\newcommand\cmmr[1]{\textcolor{red}{@MR #1}}
\newcommand{\bigO}{\mathcal{O}}

\graphicspath{{img/}}

\begin{document}

\title{Reconstruction of surfaces given by point clouds}

\author{
\IEEEauthorblockN{
Viktor Cvrtila\IEEEauthorrefmark{1},
Miha Rot\IEEEauthorrefmark{1}\IEEEauthorrefmark{2},
}

\IEEEauthorblockA{\IEEEauthorrefmark{1} %1st affiliation 
Institut "Jožef Stefan", Department of Communication Systems, Jamova cesta 39, 1000 Ljubljana, Slovenia}
\IEEEauthorblockA{\IEEEauthorrefmark{2}Jozef Stefan International 
	Postgraduate School, Jamova cesta 39, 1000 Ljubljana, Slovenia}

Viktor.Cvrtila@ijs.si

%\thanks{ applicable sponsor/s here. If no sponsors, delete this Latex command}
}

\maketitle

\begin{abstract}
%In this paper we propose an algorithm that reconstructs the boundary and interior of a three-dimensional domain given as a point cloud describing the boundary.
One of the most general ways to represent a three-dimensional domain
is with a dense point cloud that describes the boundary.
This representation is convenient as it is both the output of a 3D scan and relatively simple to obtain from alternative surface description methods. %\cmmr{Ok?}
%This representation is convenient, as it is other representations can be converted into point clouds.
However, a point cloud on its own is often insufficient for further calculations.
We would like to use said point cloud to create a more convenient description of
the true shape, which would allow us to use specialised discretization algorithms, and a way to determine its interior.
%On one hand, it may be uneven or contain gaps.
%On the other hand,
%there is no natural way of determining if a given point is in the interior of the domain,
%represented by the point cloud.
In this paper we propose an algorithm that fits parametrized surfaces to discrete neighbourhoods that cover the point cloud and uses a partition of unity to ensure that the surfaces match along their edges.
We then use this local parametrization to construct the characteristic function
of the domain, i.e. a function which can determine if a given point is inside the domain or not.
 
\end{abstract}

\renewcommand\IEEEkeywordsname{Keywords}
\begin{IEEEkeywords}
\textit{surface reconstruction, point clouds, meshless methods}
\end{IEEEkeywords}

\section{INTRODUCTION}

Many real-world problems can be expressed mathematically as a system of partial differential equations (PDEs).
Often, there are no known analytical solutions to such problems.
Knowing how to numerically solve PDEs is crucial in many areas,
from meteorology~\cite{marchuk2012weather} to modelling of gas turbines~\cite{benaarbia2018turbine}.
In recent years, much work has been done in the field of meshless numerical solvers \cite{shankar2020manifolds, tominec2021unfitted, medusa}.
In contrast to other methods - like the finite element method that requires a triangularization of the computational domain - these solve PDEs on a set of scattered nodes.
%In contrast, other methods require more information about the domain.
%The finite element method, the prototypical method using meshes,
%requires a triangularization of the domain, for example.
%There are many meshless methods, e.g.~the weak form MLPG~\cite{DepolliTrobec2019} or the strong form RBF-FD~\cite{SBGRBF}\cmmr{Tole je malo random, se Gregor strinja, da lahko odstraniva.}.
In practice, there are many different ways to present the domains of PDEs.
%When the domain is complex, as is the case when simulating properties of real-world objects, the need for more complex representations arises.
%Numerical solvers often accept triangle meshes or NURBS patches.
Triangle meshes or non-uniform rational basis spline (NURBS)
%\cmmr{Pomoje bi tukaj pasal citat (ne se ravno matrat, ce ne najdes hitro)}
patches~\cite{nurbsCite} are common choices but both come with unique caveats.
%These do not come without problems;
The first is computationally expensive to produce and is not suited for meshless methods while the latter cannot be used directly and requires further processing, e.g.~by converting it to a mesh \cite{AdanCardoso2020}
or by discretization \cite{DKS2021}.

This paper focuses on another representation -- \textit{point clouds}.
A point cloud is a finite set of points $X \subset \R^d$.
Point clouds can be measured (e.g.~when using 3D scanners) or produced during computation when discretizing a known domain.
This typically produces point clouds $X \subset \partial D$ that describe
the boundary of some domain $D \subset \R^d$.
Working with point clouds directly would allow for more flexibility when working with complex domains.
Unfortunately, point clouds carry less data than other representations and introduce new problems.
Namely, for a point cloud $X \subset \partial D$
\begin{itemize}
	\item there is no natural way to \textit{rediscretize} the surface $\partial D$.
	In other words there is no way to make the point cloud more or less dense.
	\item There is no natural way to determine if a given point is inside the domain $D$.
\end{itemize}

The goal of this paper is to present an algorithm that alleviates these problems.
This algorithm differs from others by creating a locally parametrized surface,
where the local parametrization maps are created using radial basis functions.
Similar work has been done in \cite{9320452},
with a focus on the visual aspects of the reconstructed surface.
They define a procedure, that takes patches of a surface
and ensures that they match near their boundaries.
It focuses primarily on how the resulting patches look,
e.g.~by making sure that the resulting patches have no holes between them.
We, on the other hand, can pick as many patches as we need, so we sidestep this problem.
Because of this we can also use a much simpler procedure to make sure the patches match.
Analogous problem has also been tackled implicitly~\cite{curlFreeRBF}, with the surface given by a point cloud reconstructed as an isosurface of a scalar field.
We will discuss why this is not the best option for our use case in section~\ref{sec:algo}.
We, however, use similar interpolants.
For an overview of other surface reconstruction methods, see \cite{SurfReconSurvey}.

In section~\ref{sec:algo} we present the algorithm, split into three parts;
we discuss the preliminary point cloud decomposition and estimations in~\ref{sec:algo:param-dom-constr}.
The creation of local parametrizations is described in~\ref{sec:algo:local-params}.
We finish by constructing the characteristic function in~\ref{sec:algo:char-map}.
The algorithm is then demonstrated on a simple two-dimensional point cloud in section~\ref{sec:usage-ex}.

\section{THE ALGORITHM}
\label{sec:algo}

Before we present the algorithm,
we explain the reasoning behind our decisions in its design.
If we ignore approximations of surfaces - such as triangular meshes -
the most common ways of defining surfaces using equations are as a parametrization or as an isosurface of a scalar field.
The latter is often simpler and more general.
It is easier to produce a scalar field such that the given points lie on
a desired isosurface; one can construct such a field using interpolation.
However, it is less convenient -- even finding a point on an
isosurface is non-trivial and typically involves solving a non-linear equation.
On the other hand, parametrization is harder to produce, but easier to work with. With a parametrized surface, we can easily use existing discretization algorithms~\cite{DKS2021} to produce nodes of desired density.
%We choose parametrization, knowing that if we do the work of constructing such a map, further work is made easier.
We choose parametrization knowing that once we invest effort into its construction, further work is made easier.
%If we also decide to construct several local parametrizations
%(cf. the definition of a topological manifold), \cmmr{Tukaj ne vem, ce mi je %res jasno kaj si zelel povedat.}
%we sidestep many topological problems.
As described, the algorithm is not dependant on the dimension $d$ of the domain $D \subseteq \R^d$.
We choose the dimension to equal $2$, as it is easier to illustrate the procedure.

%\cmmr{A lahko nekje omenimo, da je sicer za splosen $d$ ampak bomo mi tukaj uporabljali in kazali 2?}

%The algorithm functions as follows.
We first give a brief overview of the algorithm.
It accepts a point cloud $X \subset \partial D$.
This point cloud should be dense enough to sufficiently describe the features of the boundary
$\partial D$.
%The algorithm
We intend to produce a series of local parametrizations $\vec p_i$ and
the characteristic function $c$.
\begin{enumerate}
	\item\label{list:1-start} First, select a \textit{discrete neighbourhood} $X_i$ for each point $\vec x_i \in X$,
	i.e. a subset $\vec x_i \in X_i \subset X$.
	Neighbouring points $\vec{x}_{i, j} \in X_i$ are typically close to $\vec x_i$.
	\item Second, calculate a rough approximation for normals $\vec v_i$ on
	$\partial D$ at each point $\vec x_i$ in $X$.
	\item\label{list:1-end} Next, create a parametrization domain $U_i \subset \R^2$
	for each discrete neighbourhood $X_i$.
	At the same time, create a series of knot vectors $\vec u_{i, j}$,
	corresponding to each point $\vec x_{i, j} \in X_i$.
	\item\label{list:2-start} Fit a function $\vec s_i\colon U_i \to \R^2$ to each discrete neighbourhood,
	such that $\vec s_i(\vec u_{i,j}) = \vec x_{i, j}$.
	\item In parallel with the previous step, construct a sufficiently smooth partition of unity
	$\phi_i\colon R^2 \to [0, 1]$ around each point $\vec x_i \in X$.
	\item\label{list:2-end} Using the functions from the previous steps,
	construct the local parametrizations
	$\vec p_i\colon U_i \to \R^2$.
	These differ from the functions $\vec s_i$, since 
	the union $\bigcup_i \vec p_i(U_i)$ is a manifold, whereas $\bigcup_i \vec s_i(U_i)$ is not.
	\item\label{list:3-start} Before we can construct the function that determines the interior,
	once again approximate normals on $\partial D$,
	this time based on the derivatives of $\vec p_i$.
	\item\label{list:3-end} Finally, construct a function $c\colon \R^2 \to \R$
	that can determine if a point is inside $D$.
\end{enumerate}
Section~\ref{sec:algo:param-dom-constr} will focus on steps~\ref{list:1-start} through~\ref{list:1-end},
section~\ref{sec:algo:local-params} will focus on steps~\ref{list:2-start} through~\ref{list:2-end}
and section~\ref{sec:algo:char-map} will cover the remaining steps.

In short, the algorithm accepts a point cloud $X \subset \partial D$ and produces
a series of local parametrizations $\vec p_i\colon U_i \to \R^2$ as well as a function $c\colon \R^2 \to \R$,
such that the sign of $c(\vec x)$ determines whether $\vec x$ is in the interior, on the boundary
or in the exterior of $D$.

At this step note two details.
We have no intention of producing a global parametrization $\vec p\colon U \to \partial D$.
By imitating the definition of a manifold, we achieve a great deal of generality
without having to mesh the surface.
We could, in principle, “glue” the parametrization domains $U_i$ together.
This would be impractical, as it would essentially create a covering space,
the topology of which could be complex.
Also, the definition of the function $c$ is fairly vague.
This is because we can choose how to construct the function.
An elegant approach would be to fit a scalar field to the points
$\vec x_i - \vec n_i, \vec x_i, \vec x_i + \vec n_i$,
such that the sign of $c(\vec x)$ determines whether $\vec x$ is in $D$,
on the boundary, or in the exterior of $D$.
The problem with this approach is that constructing $c$ in such a way,
that it agrees with the parametrizations $\vec p_i$, is not trivial.
If we need to work with both the boundary and the interior,
we can use another procedure.
When given a point $\vec x$, we can find the nearest point on $\partial D$ to $\vec x$
and decide based on the corresponding normal.

\subsection{CONSTRUCTING THE PARAMETRIZATION DOMAINS}
\label{sec:algo:param-dom-constr}

One of the main problems of fitting a parametrized surface to a point cloud
is the fact that we do not have any obvious choice of domain.
That is, the choice of knot vectors $\vec u_{i, j}$ is not obvious.
This problem is solved in the first three steps of the algorithm.
Since we decided to construct only local parametrizations,
we can restrict ourselves to smaller domains, such that on each $U_i$,
the image $\vec p_i(U_i)$ is a graph of a function, allowing for rotations.

To achieve this, we first select a small enough discrete neighbourhood
for each point $\vec x_i$ from the point cloud $X$.
Small enough, here, means that there should exist a connected neighbourhood of the
discrete neighbourhood in $\partial D$, such that this neighbourhood
is a graph of a function, up to rotation.
%We will later fit a local parametrization to each such set of points.
We intend to fit a local parametrization to each such set at a later step.
The simplest way to achieve this is by taking the neighbourhood of $\vec x_i$
to be its $(k-1)$-nearest neighbours for a small enough $k$.
Let $\vec x_i$ be a point in $X$. Write
$\{\vec x_i = \vec x_{i, 1}, \vec x_{i, 2}, \ldots, \vec x_{i, k}\} = X_i \subset X$.
One way of constructing these would be using a $k$-d tree \cite{bentley1975kdtree} to find the required points.
After constructing a $k$-d tree, querying the $k$ nearest points to a point $\vec x_i$
for some small fixed $k$ is computationally inexpensive. This method also lends itself well to parallel computing, as demonstrated in \cite{TrobecDepolli2021}.

Next, we wish to approximate the normals $\vec n_i$ on $\partial D$ at $\vec x_i$.
There are many ways to do this, e.g.~using Voronoi covariance measures~\cite{normalsVCM}, or jets~\cite{normalsJets}.
We use a simpler approach -- we fit a plane to $X_i$ using the least squares method, since this is a robust method and easy to implement and understand.
This does not assume that the discrete neighbourhoods are almost flat.
At this stage, we just need an approximation of the normal.
%\cmmr{A je to ok, ker so majhne (torej skoraj flat) ploskve? Ce ja a bi to se dodalv komentar?}
We can chose the normal of the hyperplane $\vec v_i$ as a first approximation of the normal.
The problem with this procedure is that we have no guarantee
that the normals are picked consistently -- some may be inward-pointing,
while others may be outward-pointing.
This does not pose a problem at this point,
as we only need the direction represented by the normals.

With the discrete neighbourhoods $X_i$ and the normals $\vec v_i$,
we can construct the parametrization domains $U_i$.
Write $P_i$ for the plane passing through $\vec x_i$ perpendicular to $\vec v_i$.
Let $\vec A_i\colon \R^2 \to \R^2$ be an affine map that maps
$P_i$ to $\R^2\times\{0\}$ and $\vec x_i$ to $(0, 0, 0)$.
We can then expect that if we apply $\vec A_i$
to a small connected open neighbourhood of $X_i$ in $\partial D$,
we get a graph of a function.
With this in mind, define $\tilde{\vec u}_{i, j}$ to be the projection of
$\vec x_{i, j}$ along $\vec v_i$ onto $P_i$.
Then define $\vec u_{i, j} \in \R^2$ as $\vec A_i \tilde{\vec u}_{i, j}$.
Since it may be convenient later, we normalize the set of knot vectors
so that the distance between $\vec u_{i, 0}$ and the nearest vector among
$\vec u_{i, 1}, \ldots, \vec u_{i, k}$ equals one.
For the moment, select the entire plane $\R^2$ as the domain $U_i$.
When we construct the parametrization maps, we can shrink
the domains $U_i$ to equal a closed unit ball.

%%%NEW
This procedure, which only has to be done once, is relitively fast for small $k$.
Constructing a $k$-d tree with $n$ points has $\bigO(n\log(n))$ time complexity,
and queriying the $k$ nearest neighbours has $\bigO(k\log(n))$.
Using the least squares method to approximate the normals is $\bigO(k^2)$ per normal.
The entire procedure is therefore $\bigO(kn\log(n) + k^2n)$.

\subsection{CONSTRUCTING THE LOCAL PARAMETRIZATIONS}
\label{sec:algo:local-params}

We now discuss steps~\ref{list:2-start} through~\ref{list:2-end}.
Since we have the knot vectors $\vec u_{i, j}$ for each discrete neighbourhood $X_i$,
fitting an interpolating surface to the points in $X_i$ is simple.
For example, let $\varphi\colon [0, \infty) \to \R$ be a radial basis function (RBF).
We can then find an interpolant of the form
$$ \vec s_i(\vec x) = \vec Q_i(\vec x) + \sum_{j = 1}^k \alpha_{i, j}\varphi(|| \vec x - \vec x_{i, j} ||), $$
where $Q_i$ are polynomials and $\alpha_{i, j}\in\R^2$,
such that for each $j = 1, \ldots, k$
$$ \vec s_i(\vec u_{i, j}) = \vec x_{i, j}. $$
For an overview of RBF interpolation, consult chapter 3 of \cite{fornbergRBFPrimer}.
The role of the polynomial $\vec Q_i$ is discussed in \cite{FLYER201621}.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{ill-overlap}
	\caption{The result of interpolating points on discrete neighbourhood $1, 2, 3$ and $2, 3, 4$ on the upper graph. The gluing operation is then applied to these maps on the lower graph.}
	\label{fig:proc-ill}
\end{figure}

The problem here is that we do not have a proper local parametrization.
The maps $\vec s_i$ only interpolate the points on the surface, so they do not agree inbetween the points;
see the upper graph in Fig.~\ref{fig:proc-ill}.
We construct two interpolating maps, $\vec s_1$ and $\vec s_2$,
which interpolate points with $x$-coordinates of $1$, $2$, $3$ and $2$, $3$, $4$ resp.
Notice that the maps $\vec s_1$ and $\vec s_2$ do not agree between the middle two points;
the union of the images of the maps do not form a manifold around these points.

This can be fixed by “gluing” the maps together using a partition of unity (PU).
A PU is a series of functions $\phi_1, \ldots, \phi_n$ such that $\sum_{i=1}^n \phi_i$
equals the constant $1$ function. If the support of these functions is small enough,
they can be used to smoothly combine locally defined functions.
A partition of unity around a set of points in $\R^2$ can be constructed as follows.
First, choose a radius $r_i > 0$ for each point $\vec x_i$ from $X$.
This should be chosen such that the ball $B(\vec u_{i, 0}, r_i)$
contains the knot vectors corresponding to the nearest points to $\vec x_i$ in $X_i$.
Then, define a function $\kappa\colon [0, \infty) \to [0, \infty)$
such that the support of $\kappa$ equals $[0, 1]$ and that the derivatives
$\kappa^{(j)}(0)$ and $\kappa^{(j)}(1)$ equal $0$ for all $0 < j \le d$.
We can use, for example, the function
\begin{equation*}
	\kappa(x) = 
	\left\{
	\begin{array}{ll}
		\exp\left(-\frac{1}{1 - x^2}\right)  & \mbox{if } x \in [0, r) \\
		0 & \mbox{if } x \in [r, \infty)
	\end{array}
	\right.
	.
\end{equation*}
Then we simply define
\begin{equation*}
	\phi_i(\vec u) = \frac{\kappa\left(\frac{||\vec u||}{r_i}\right)}{\sum_{j = 1}^{n} \kappa\left(\frac{||\vec u||}{r_j}\right)}.
\end{equation*}

%Now we construct the parametrization maps.
The parametrizarion maps $\vec p_i$ are then evaluated as follows.
Suppose we want to calculate $\vec p_i(u)$ for some $\vec u \in \R^2$.
First, we calculate $\vec s_i(\vec u)$, as the first approximation.
Write $\vec x^{(0)} = \vec s_i(\vec u)$.
Then find all $\vec x_j$ from $X$, such that the distance between $\vec x^{0}$ and $\vec x_j$ is less than $r_i$.
%We could, for example, query $X$ for all points
%that are at most $\max_{j=1,\ldots,n} r_j$ away from $x_i$.
Write $\vec x_{i_1}, \ldots, \vec x_{i_\ell}$ for these points.
Now, for each index $j = 1,\ldots, \ell$ project the point $\vec x^{(0)}$ along $\vec v_{i_j}$
onto the plane $P_{i_j}$ and applying the maps $\vec A_{i_j}$ to get $\vec u^{(0)}_1, \ldots, \vec u^{(0)}_\ell$;
note that we repeated the procedure for calculating the knot vectors $\vec u_{i, j}$,
but using the point $\vec x^{(0)}$.
Now we define the value of $\vec p_i$ at $\vec u$ by
\begin{equation*}
	\vec p_i(\vec u) = \sum_{j = 1}^{\ell} \vec s_{i_j}(\vec u^{(0)}_j)\phi_{i_j}(\vec u^{(0)}_j).
\end{equation*}
The lower graph in Fig.~\ref{fig:proc-ill} shows the result of the gluing procedure,
when applied to the functions displayed in the upper graph.

%%%NEW
Since we have all the data from section~\ref{sec:algo:param-dom-constr},
each evaluation of a parametric map is mostly dependant on the size of the discrete neighbourhoods $k$.
Calculating the coefficients $\alpha_{i, j}$ for each interpolant $\vec s_i$ is $\bigO(k^3n)$.
If we choose the radiuses $r_i$ such that each ball with center $x_i$ and radius $r_i$ contains at most $k$
other points of $X$, one evaluation is $\bigO(k^2 + k\log(n))$.

\subsection{CONSTRUCTING THE CHARACTERISTIC FUNCTION}
\label{sec:algo:char-map}

Our final task is to construct an approximation of the characteristic map.
To do so, we must at this point compute better approximations for the normals.
Since we have the parametrization maps $\vec p_i$,
this is much simpler than in~\ref{sec:algo:param-dom-constr}.
Let $\vec n^{(0)}_i$ equal the normalized cross product of the derivatives of $\vec p_i$ at $(0, 0)$.
Such a vector is indeed a normal on the image of $\vec p_i$.
The problem here is that the normals $\vec n^{(0)}_i$ may not all face in the same direction.
Some may be inward-facing, while others may be outward-facing.
We must ensure that the choice of normal is consistent.
There are many ways to achieve this.

One of the simpler ways is to propagate the direction of a given normal
breadth-first to its nearest neighbours.
Choose a normal $\vec n_i = \vec n^{(0)}_i$ and add its index $i$ to a FIFO queue.
Take the discrete neighbourhood $X_i$ of $\vec x_i$.
Let $\vec n^{(0)}_{i, j}$ be the normal at $\vec x_{i, j}$;
that is, there exists some index $m$ such that
$\vec x^{i, j} = \vec x_m$, so $\vec n^{(0)}_{i, j} = \vec n^{(0)}_m$.
%If we have not yet ensured it points in the correct direction,
If we have not visited it during the procedure,
check if the dot product $\langle \vec n^{(0)}_m, \vec n^{(0)}_i\rangle$ is positive.
If it is, set $\vec n_m = \vec n^{(0)}_m$, otherwise set $\vec n_m = - \vec n^{(0)}_m$.
Then add the index $m$ to the queue and repeat until the queue is empty.
This procedure ensures that the normals are chosen consistently.
However, we do not at this point know whether the normals are all inward- or outward-facing.
We can solve this problem with the help of the characteristic function.

Suppose we are not interested in the local parametrizations, but only in the characteristic function.
We can construct a function, whose sign determines the position of a point in relation to the domain.
Using RBFs, we can create a map $c\colon \R^2 \to \R$ by interpolating
\begin{equation}
	\label{eq:chi-interp-conditions}
	c(\vec x_i - \vec n_i) = -1\text{, } c(\vec x_i) = 0 \text{ and } c(\vec x_i + \vec n_i) = 1.
\end{equation}
If we wish to interpolate only locally, we may construct a function $c_i$
satisfying \eqref{eq:chi-interp-conditions} for all points in a discrete neighbourhood $X_i$.
Then we simply combine these using an appropriate partition of unity around
the points of $X$. If $\phi_1, \ldots, \phi_n$ is such a partition of unity,
define $c = \sum_{i=1}^{n} c_i\phi_i$.
The advantage of using the normal vector approximations $\vec n_i$ over the vectors $\vec v_i$
from~\ref{sec:algo:param-dom-constr} is that the former are smoother in a way,
since they were constructed using the smooth local parametrization maps.
%Note that we could have used the vectors $v_i$ from section~\ref{sec:algo:param-dom-constr},
%if we normalized them and ensured that their direction is chosen consistently.
%If we used the direction-propagation algorithm from this section on the vectors
%$v_1, \ldots, v_n$, we could replace the vectors $n_i$ in \eqref{eq:chi-interp-conditions}
%with $v_i$'s.
%However, the normals obtained using the local parametrizations are better,
%as the parametrizations are smoother.

If, on the other hand, we want both the reconstructed boundary and the characteristic function,
we employ the following procedure.
We again construct a function $c\colon \R^2 \to \R$.
Say we want to calculate $c(\vec x)$ for some $\vec x\in \R^2$.
First find the nearest point to $\vec x$ in the point cloud $X$, say $\vec x_i$.
Write $\vec x^{(0)}$ for an approximation of the point that minimizes
the distance between $\vec x$ and the image %$\vec p_i(B(0, 1))$.
of the unit ball under $\vec p_i$.
We do not need a particularly good approximation,
so we use a fixed small number of iteration steps of Newton's method.
Let $\vec n^{(0)}$ be the normal at $\vec x^{(0)}$.
Then define $ \vec c(x) = \langle \vec x^{(0)} - \vec x, \vec n^{(0)} \rangle$.

If we do indeed take a small fixed number of iterations, evaluating the function $c$
is still in $\bigO(k^2 + k\log(n))$

The sign of both of these functions determines the position of a point $\vec x$.
If $c(\vec x)$ is negative, then $\vec x \in D$.
If it equals zero, then $\vec x$ is on the boundary $\partial D$.
Otherwise, $\vec x$ is outside of $\vec D$.

We can also use these maps to ensure that the normals all point in the right direction.
Take a point $\vec x_0 \in D$, i.e.~a point that we know to be inside the domain,
and calculate $c(\vec x_0)$. If this value is positive, flip all the $\vec n_i$ vectors.



\section{USAGE EXAMPLE}
\label{sec:usage-ex}

Consider the polar curve given by the equation
\begin{equation}
	\label{eq:polar-ex}
	r(\theta) = 1 - \cos(\theta)\sin(\theta).
\end{equation}
To test our algorithm,
we first naively discretize the curve by taking the points corresponding to
$\theta = 0, \frac{\pi}{50}, \frac{2\pi}{50}, \ldots, \frac{49\pi}{50}$.
We get a set of fifty unevenly spaced points $X$; see Fig.~\ref{fig:ex-start}.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{ex-start-pts}
	\caption{The example point cloud produced from sampling curve \eqref{eq:polar-ex}.}
	\label{fig:ex-start}
\end{figure}
We choose neighbourhoods of size $k = 5$.
First, we approximate the normals as stated in~\ref{sec:algo:param-dom-constr}.
Note that at this stage, the normals are not consistent.
After constructing the parametrization domains,
we construct the interpolation maps.
As before, they do not agree; they interpolate each series of
consecutive five points, but the union of their images
does not form a manifold.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{ex-bad-fill}
	\caption{Each curve represents the image of a map $s_i$ for $i = 1,\ldots, 50$.}
	\label{fig:ex-interp}
\end{figure}
See Fig.~\ref{fig:ex-interp} (or Fig.~\ref{fig:proc-ill}).
Now, we construct the parametrization maps $p_i$.
These are actual local parametrizations of a manifold.
The reconstructed curve is displayed in Fig.~\ref{fig:ex-param}.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{ex-good-fill}
	\caption{The union of the images of $p_i$ for $i = 1, \ldots, 50$.}
	\label{fig:ex-param}
\end{figure}
We finally construct the map $c$ using both methods described above.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{ex-int}
	\caption{The result of a naive fill algorithm using the $c$ map.
		%\cmmr{Mogoce bi res lepo zgledalo z zunanjimi v rdecem?}
	}
	\label{fig:ex-in}
\end{figure}
We use a naive interior fill algorithm to check if the map $c$ functions as intended.
We take points on a grid and, using $c$, we check if they are in the interior of the domain.
Fig.~\ref{fig:ex-in} displays the results of this fill algorithm.

%\section{INTRODUCTION}
%
%These instructions give you guidelines for typing camera‑ready papers for the 45\textsuperscript{th} Jubilee International Convention MIPRO 2022.
%
%The latest version of this template can be found on GitHub at URL \url{https://github.com/sgros/mipro-template}. In case you have a problem, or or you want to contribute to this template, please use GitHub's issue tracker and pull requests.
%
%The paper should consist of a title, author's name(s), affiliation, abstract, keywords, introduction, main text with section titles and subheadings (if any), conclusion, acknowledgment (if any), references and optional appendices. The length of the paper is limited to six pages including illustrations.
%
%Your goal is to simulate the usual appearance of papers in an IEEE conference proceedings. The authors' affiliations should appear immediately following their names.
%
%This electronic document is a “live” template and is used to format your paper and style the text. The template provides authors with most of the formatting specifications needed for preparing electronic versions of their papers. The various components of your paper (title, text, heads, etc.) are already defined on the style sheet, as illustrated by the portions given in this document. All margins (top and bottom margin of 25 mm, and left and right margin of 20 mm), column widths (of 82mm with the space between the two columns of 6mm), line spaces, and text fonts are prescribed; please do not alter them.
%
%\subsection{Full-Sized Camera-Ready (CR) Copy}
%
%Times New Roman font are strictly required. Follow the type sizes specified in Table~\ref{type_sizes} (expressed in points). There are 72 points per inch, and 1 point is about 0.35 mm.
%
%\begin{table}[h]
%\renewcommand{\arraystretch}{1.3}
%\caption{Type Size for Camera-Ready Papers}
%\label{type_sizes}
%\centering
%\begin{tabular}{|c|p{10em}|c|c|} 
% \hline
% \multirow{2}{*}{Type size} & \multicolumn{3}{|c|}{Appearance} \\
% \cline{2-4}
% & Regular & Bold & Italic \\
% \hline\hline
% 8 & Section titles, references, tables, table names, first letters in table captions, figure captions, footnotes, text subscripts and superscripts &  &  \\ 
% \hline
% 9 &  & Abstract,keywords & \\
% \hline
% 10 & Authors’ affiliations, main text, equations, first letters in section titles & & Subheading \\
% \hline
% 11 & Author's names & & \\
% \hline
% 24 & Paper title & & \\ 
% \hline
%\end{tabular}
%\end{table}
%
%Prepare your camera‑ready paper on the A4 paper size (210 mm x 297 mm). You are not allowed to use US letter-sized paper.
%
%Justify both left and right columns. On the last page of your paper, adjust the lengths of the columns so that they are equal. Use automatic hyphenation and check spelling. Do not add page numbers.
%
%\section{HELPFUL HINTS}
%
%\subsection{Abbreviations and Acronyms}
%
%Define abbreviations and acronyms the first time they are used in the text, even after they have been defined in the abstract. Abbreviations such as IEEE, SI, MKS, CGS, sc, dc, and rms do not have to be defined. Do not use abbreviations in the title or heads unless they are unavoidable.
%
%\subsection{Units}
%
%\begin{itemize}
%    \item Use either SI (MKS) or CGS as primary units. (SI units are encouraged.) An exception would be the use of English units as identifiers in trade, such as “3.5-inch disk drive”.
%    \item Avoid combining SI and CGS units, such as current in amperes and magnetic field in oersteds. This often leads to confusion because equations do not balance dimensionally. If you must use mixed units, clearly state the units for each quantity that you use in an equation.
%    \item Do not mix complete spellings and abbreviations of units: “Wb/m2” or “webers per square meter”, not “webers/m2”. Spell out units when they appear in text: “. . . a few henries”, not “. . . a few H”.
%    \item Use a zero before decimal points: “0.25”, not “.25”. Use “cm3”, not “cc”. 
%\end{itemize}
%
%\subsection{Figures and Tables}
%
%Place figures and tables at the top and bottom of columns. Avoid placing them in the middle of columns. Large figures and tables may span across both columns. Figure captions should be below the figures; table heads should appear above the tables. Insert figures and tables after they are cited in the text. Use the abbreviation “Fig. 1”, even at the beginning of a sentence.
%
%\begin{figure}
%  \label{fig:figure1}
%  \centering
%  \includegraphics{figure1.jpg}
%  \caption{Magnetization as a function of applied field. Note how the caption is centered in the column}
%\end{figure}
%
%Use words rather than symbols or abbreviations when writing Figure axis labels to avoid confusing the reader. As an example, write the quantity “Magnetization”, or “Magnetization, M”, not just “M”. If including units in the label, present them within parentheses. Do not label axes only with units. In the example, write “Magnetization (A/m)” or “Magnetization {A[m(1)]}”, not just “A/m”. Do not label axes with a ratio of quantities and units. For example, write “Temperature (K)”, not “Temperature/K”.
%
%\subsection{Equations}
%
%Number equations consecutively. Equation numbers, within parentheses, are to position flush right, as in (1), using a right tab stop. To make your equations more compact, you may use the solidus ( / ), the exp function, or appropriate exponents. Italicize Roman symbols for quantities and variables, but not Greek symbols. Use a long dash rather than a hyphen for a minus sign. Punctuate equations with commas or periods when they are part of a sentence, as in
%
%\begin{equation}
%    \alpha + \beta = \chi
%\end{equation}
%
%Note that the equation is centered using a center tab stop. Be sure that the symbols in your equation have been defined before or immediately following the equation. Use “(1)”, not “Eq. (1)” or “equation (1)”, except at the beginning of a sentence: “Equation (1) is . . .”
%
%\subsection{Some Common Mistakes}
%
%\begin{itemize}
%    \item The word “data” is plural, not singular.
%    \item The subscript for the permeability of vacuum $\epsilon_0$, and other common scientific constants, is zero with subscript formatting, not a lowercase letter “o”.
%    \item In American English, commas, semi-/colons, periods, question and exclamation marks are located within quotation marks only when a complete thought or name is cited, such as a title or full quotation. When quotation marks are used, instead of a bold or italic typeface, to highlight a word or phrase, punctuation should appear outside of the quotation marks. A parenthetical phrase or statement at the end of a sentence is punctuated outside of the closing parenthesis (like this). (A parenthetical sentence is punctuated within the parentheses.)
%    \item A graph within a graph is an “inset”, not an “insert”. The word alternatively is preferred to the word “alternately” (unless you really mean something that alternates).
%    \item Do not use the word “essentially” to mean “approximately” or “effectively”.
%    \item In your paper title, if the words “that uses” can accurately replace the word “using”, capitalize the “u”; if not, keep using lower-cased.
%    \item Be aware of the different meanings of the homophones “affect” and “effect”, “complement” and “compliment”, “discreet” and “discrete”, “principal” and “principle”.
%    \item Do not confuse “imply” and “infer”.
%    \item The prefix “non” is not a word; it should be joined to the word it modifies, usually without a hyphen.
%    \item There is no period after the “et” in the Latin abbreviation “et al.”.
%    \item The abbreviation “i.e.” means “that is”, and the abbreviation “e.g.” means “for example”.
%\end{itemize}
%
%An excellent style manual for science writers is \cite{young2002technical}.
%
%If your native language is not English, try to get a native English‑speaking colleague, or somebody fluent in English to proofread your paper. Use grammar existent in text editor.
%
%\subsection{References}
%
%The template will number citations consecutively within brackets \cite{eason1955certain}. The sentence punctuation follows the bracket \cite{maxwell1873treatise}. Refer simply to the reference number, as in \cite{jacobs1963fine}—do not use “Ref. \cite{jacobs1963fine}” or “reference \cite{jacobs1963fine}” except at the beginning of a sentence: “Reference \cite{jacobs1963fine} was the first . . .”
%
%Number footnotes separately in superscripts. Place the actual footnote at the bottom of the column in which it was cited. Do not put footnotes in the reference list. Use letters for table footnotes.
%
%Unless there are six authors or more give all authors' names; do not use “et al.”. Papers that have not been published, even if they have been submitted for publication, should be cited as “unpublished” \cite{elissa}. Papers that have been accepted for publication should be cited as “in press” \cite{nicole}. Capitalize only the first word in a paper title, except for proper nouns and element symbols.
%
%For papers published in translation journals, please give the English citation first, followed by the original foreign-language citation \cite{yorozu1987electron}.
%
%\subsection{Other Recommendations}
%
%The Roman numerals are used to number the section headings. Do not number ACKNOWLEDGMENTS and REFERENCES, and begin Subheadings with letters. Use two spaces after periods (full stops). Hyphenate complex modifiers: “zero-field-cooled magnetization.” Avoid dangling participles, such as, “Using (1), the potential was calculated.” Write instead, “The potential was calculated using (1),” or “Using (1), we calculated the potential.”

\section{Conclusion}
\label{sec:conclusion}

The focus of this paper was to introduce a parametric surface algorithm based on local RBF interpolation.
When given a point cloud, the algorithm creates a series of local parametrizations around each point and joins them into a smooth manifold by using the partition of unity.
It also creates the characteristic function of the reconstructed surface
based on the local parametrizations.
Such an algorithm enables one to work with discretized surfaces -- to rediscretize them or to discretize their interiors.
While originally intended for surface reconstruction in two dimensions, it is not dependant on dimension. %\cmmr{Preveri dimenzije in diskusijo.}
It can be easily modified to work on hypersurfaces of any dimension, 
due to the use of RBF interpolants.
Further work would need to focus on testing the algorithm on more complex examples and direct comparison with alternative approaches.
%Further work would need to focus on analysing the time complexity of the algorithm.
%\cmmr{Pa kaksno analizo delovanja na zajebanih primerih in visjih dimenzijah.}

\section*{acknowledgment}
The authors would like to acknowledge the financial support of the Slovenian Research Agency (ARRS) research core funding No.\ P2-0095, Young Researcher program PR-10468, and project funding No.\ J2-3048.

%\appendix


\bibliographystyle{IEEEtran}
\bibliography{references}

\end{document}
