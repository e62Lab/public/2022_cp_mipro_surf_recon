\documentclass{MIPRO}

\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage[T1]{fontenc}
\usepackage{flushend}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{array}
\usepackage{url}

\newcommand{\R}{\mathbb{R}}

\begin{document}

\title{Reconstruction of surfaces given by point clouds}

\author{
\IEEEauthorblockN{
V. Cvrtila\IEEEauthorrefmark{1},
G. Kosec\IEEEauthorrefmark{1},
}

\IEEEauthorblockA{\IEEEauthorrefmark{1} %1st affiliation 
Institut "Jožef Stefan", Department of Communication Systems, Ljubljana, Slovenia}

Viktor.Cvrtila@ijs.si

\thanks{ applicable sponsor/s here. If no sponsors, delete this Latex command}
}

\maketitle

\begin{abstract}
In this paper we propose an algorithm that reconstructs the boundary and interior of a three-dimensional domain given as a point cloud describing the boundary. One of the most general ways to represent a three-dimensional domain is to give its boundary in the form of a dense point cloud. This is what we are given when using a 3D scanner, for example. However, such a point cloud on its own is often insufficient for further calculations. On one hand, it may be uneven or contain gaps. On the other hand, there is no natural way of determining if a given point is in the interior of the domain, represented by the point cloud. The proposed algorithm fits parametrized surfaces to discrete neighbourhoods that cover the point cloud, then uses a partition of unity to ensure the surfaces match along their edges. We can then use these parametrized surfaces to determine the interior of the domain. The proposed algorithm enables enables us to work with more types of surfaces at once, as many representations of surfaces (e.g. those using NURBS, triangle meshes) can be reduced to point clouds.
 
\end{abstract}

\renewcommand\IEEEkeywordsname{Keywords}
\begin{IEEEkeywords}
\textit{surface reconstruction, point clouds}
\end{IEEEkeywords}

\section{INTRODUCTION}

Generally, numerical solvers (e.g. DE solvers) require representations of the domains on which the problem is being solved.
When the domain is especially complex, as is the case when simulating properties of real-world objects,
the need for more complex representations arises.
For example, numerical solvers often accept triangle meshes or NURBS patches.
These do not come without problems;
the first is computationally expensive to produce and is not suited for meshless methods.
The latter cannot be used directly and requires further processing.

This paper will focus on another representation -- \textit{point clouds}.
A point cloud is simply a finite set of points $X \subset \R$.
Point clouds can be given as input data (e.g. when using 3D scanners) or produced during computation
when discretizing a known domain.
This typically produces point clouds $X \subset \partial D$ that describe
the boundary of some domain $D \subset \R^3$.
Working with point clouds directly would therefore allow for more flexibility when working
with complex domains.
Since point clouds carry less data than other representations,
we encounter new problems.
Namely, for a point cloud $X \subset \partial D$
\begin{itemize}
	\item there is no natural way to \textit{rediscretize} the point cloud.
	I.e. there is no way to make the point cloud more or less dense.
	\item there is no natural way to determine, if a given point is inside the domain $D$.
\end{itemize}

The goal of this paper is to present an algorithm that alleviates these problems.

\section{THE ALGORITHM}

The algorithm functions as follows.
It accepts a point cloud $X \subset \partial D$.
This point cloud should be dense enough to sufficiently describe the features of the boundary
$\partial D$.
The algorithm
\begin{enumerate}
	\item first selects a \textit{discrete neighbourhood} for each point $x_i \in X$,
	i.e. a subset $x_i \in X_i \subset X$.
	These points will typically be close to $x_i$.
	\item Second, it calculates rough approximation for normals $n_i$ on
	$\partial D$ at each point $x_i$ in $X$.
	\item Next, it creates a parametrization domain $U_i \subset \R^2$
	for each discrete neighbourhood $X_i$.
	At the same time, it creates a series of knot vectors $u_{i, j}$,
	corresponding to each point $x_{i, j} \in X_i$.
	\item Then it fits a function $s_i: U_i \to \R^3$ to each discrete neighbourhood,
	such that $s_i(u_{i,j}) = x_{i, j}$.
	\item With the previous step, it constructs a sufficiently smooth partition of unity
	$\phi_i: R^3 \to [0, 1]$ around each point $x_i \in X$.
	\item using the functions from the previous steps, it constructs local parametrizations
	$p_i: U_i \to \R^3$.
	These differ from the functions $p^0_i$, since they agree where they intersect.
	I.e. the union $\bigcup_i p_i(U_i)$ is a manifold, whereas $\bigcup_i p^0_i(U_i)$ is not.
	\item Before we can construct the characteristic function $\chi_D$,
	we once again approximate normals on $\partial D$,
	this time based on the derivatives of $p_i$.
	\item Finally, the algorithm constructs a function $c: \R^3 \to \R$ that can determine if a point is inside $D$.
\end{enumerate}

In short, the algorithm accepts a point cloud $X \subset \partial D$ and produces
a series of local parametrizations $p_i: U_i \to \R^3$ as well as a function $c: \R^3 \to \R$,
such that the sign of $c(x)$ determines whether $x$ is in the interior, on the boundary
or in the exterior of $D$.

At this step note two details.
We have no intentions of producing a global parametrization $p: U \to \partial D$.
By imitating the definition of a manifold, we achieve a great deal of generality.
We could, in principle, “glue” the parametrization domains $U_i$ together.
This would be impractical, as it would essentially create a covering map.
Also, the the definition of the function $c$ is fairly vague.
This is because we can choose how to construct the function.
An elegant approach would be to fit a scalar field to the points $x_i - n_i, x_i, x_i + n_i$,
such that the sign of $c(x)$ determines whether $x$ is in $D$,
on the boundary, or in the exterior of $D$.
The problem with this approach is that constructing $c$ in such a way,
that it agrees with the parametrizations $p_i$, is not trivial.
If we need to work with both the boundary and the interior,
we can use another procedure.
When given a point $x$, we can find the nearest point on $\partial D$ to $x$
and decide based on the corresponding normal.

We now discuss each step of the algorithm. Before we do so, we first explain the reasoning
behind our decisions.
If we ignore approximations of surfaces, such as triangular meshes,
the most common methods of defining surfaces using equations are
parametrizing the surface and giving it as a isosurface of a scalar field.
The second one is often simpler and more general.
It is also easier to produce a scalar field such that given points lie on
a desired isosurface; one has to only interpolate the points.
However, it is less convenient for several uses - even finding a point on an
isosurface is nontrivial.
The first option has the oposite properties in this sense.
It is harder to produce, but easier to work with.
We choose parametrization, knowing that if we do the work
of constructing such a map, further work will be made simpler.
If we also decide to construct several local parametrizations
(c.f. the definition of a topological manifold),
we sidestep many topological problems.
The main task, then, is to construct these parametrizations.

\subsection{CONSTRUCTING THE PARAMETRIZATION DOMAINS}

One of the main problems of fitting a parametrized surface to a point cloud
is the fact that we do not have any obvious choice of domain.
I.e. the choice of knot vectors $u_{i, j}$ is not obvious.
Since we decided to construct only local parametrizations,
we can restrict ourselves to smaller domains, such that on each $U_i$,
the image $p_i(U_i)$ is a graph of a function, allowing rotations.

To achieve this, we first select a discrete neighbourhood for each point $x_i$ from the point cloud $X$.
We will later fit a local parametrization to each such set of points.
To get the desired property, we select a small enough nbhd.
The simplest way to achieve this is by taking the nbhd.~of $x_i$ to be its $(k-1)$-nearest neighbours
for a small enough $k$.
Let $x_i$ be a point in $X$. Write $\{x_i = x_{i, 1}, x_{i, 2}, \ldots, x_{i, k}\} = X_i \subset X$.

Next, we wish to approximate the normals $n_i$ on $\partial D$ at $x_i$.
We can use, for example, principle component analysis.
For a point $x_i = x_{i,1}$ define the covariance matrix as
$$ \Sigma_i = \sum_{j=2}^{k} (x_{i, j} - x_{i, 1})\cdot(x_{i, j} - x_{i, 1})^T.$$
Let $s_i$ be the left singular vector corresponding to the smallest singular value of $M_i$.
We chose this to be the first approximation of the normal $n_i$.
The problem with this procedure is that we have no guarantee
that the normals are picked consistently -- some may be inward-pointing,
while others may be outward-pointing.
This does not pose a problem at this point,
as we only need the direction represented by the normals.

Now that we have the discreet nbhds.~$X_i$ and the normals $s_i$,
we can construct the parametrization domains $U_i$.
Suppose that we did indeed choose small enough discreet nbhds.
Write $P_i$ for the plane passing through $x_i$ perpendicular to $s_i$.
Let $A_i: \R^3 \to R^3$ be an affine map that maps $P_i$ to $\R^2\times\{0\}$ and
$x_i$ to $(0, 0, 0)$.
We can then expect that if we apply $A_i$ to a small connected nbhd. of $X_i$ in $\partial D$,
we get a graph of a function.
With this in mind, define $\tilde{u}_{i, j}$ to be the projection of $x_{i, j}$ along $s_i$
onto $P_i$. Then define $u_{i, j} \in \R^2$ as $A_i \tilde{u}_{i, j}$.
For the moment, select the entire plane $\R^2$ as the domain $U_i$.
We will have to shrink this later.

\subsection{CONSTRUCTING THE LOCAL PARAMETRIZATIONS}

Now that we have the knot vectors $u_{i, j}$ for each discreet nbhd.~$X_i$,
fitting a parametrized surface tho the points in $X_i$ is simple.
For example, let $\varphi: [0, \infty) \to \R$ be a radial basis function (RBF).
We can then find an interpolant of the form
$$ s_i(x) = p_i(x) + \sum_{j = 1}^k \alpha_{i, j}\varphi(|| x - x_{i, j} ||), $$
where $p_i$'s are low degree polynomials and $\alpha_{i, j}\in\R^3$,
such that for each $j = 1, \ldots, k$
$$ s_i(u_{i, j}) = x_{i, j}. $$

The problem here is that we do not have a proper local parametrization.
The maps $s_i$ only interpolate the points on the surface,
so they do not agree.
This can be fixed by “gluing” the maps together using a partition of unity.
A partition of unity around a set of points in $\R^3$ can be easily constructed.
First, choose a radius $r_i > 0$ for each point $x_i$ from $X$.
Define a function $\kappa: [0, \infty) \to [0, \infty)$
such that the support of $\kappa$ equals $[0, 1]$ and that the derivatives
$\kappa^{(j)}(0)$ and $\kappa^{(j)}(1)$ equal $0$ for all $0 < j \le d$.
We can use, for example, the function
\begin{equation*}
	\kappa(x) = 
	\left\{
	\begin{array}{ll}
		\exp\left(-\frac{1}{1 - x^2}\right)  & \mbox{if } x \in [0, r) \\
		0 & \mbox{if } x \in [r, \infty)
	\end{array}
	\right.
	.
\end{equation*}
Then we simply define
\begin{equation*}
	\phi_i(x) = \frac{\kappa\left(\frac{|| x - x_i ||}{r_i}\right)}{\sum_{j = 1}^{n} \kappa\left(\frac{|| x - x_j ||}{r_j}\right)}.
\end{equation*}

Now we construct the parametrization maps. 

\section{INTRODUCTION}

These instructions give you guidelines for typing camera‑ready papers for the 45\textsuperscript{th} Jubilee International Convention MIPRO 2022.

The latest version of this template can be found on GitHub at URL \url{https://github.com/sgros/mipro-template}. In case you have a problem, or or you want to contribute to this template, please use GitHub's issue tracker and pull requests.

The paper should consist of a title, author's name(s), affiliation, abstract, keywords, introduction, main text with section titles and subheadings (if any), conclusion, acknowledgment (if any), references and optional appendices. The length of the paper is limited to six pages including illustrations.

Your goal is to simulate the usual appearance of papers in an IEEE conference proceedings. The authors' affiliations should appear immediately following their names.

This electronic document is a “live” template and is used to format your paper and style the text. The template provides authors with most of the formatting specifications needed for preparing electronic versions of their papers. The various components of your paper (title, text, heads, etc.) are already defined on the style sheet, as illustrated by the portions given in this document. All margins (top and bottom margin of 25 mm, and left and right margin of 20 mm), column widths (of 82mm with the space between the two columns of 6mm), line spaces, and text fonts are prescribed; please do not alter them.

\subsection{Full-Sized Camera-Ready (CR) Copy}

Times New Roman font are strictly required. Follow the type sizes specified in Table \ref{type_sizes} (expressed in points). There are 72 points per inch, and 1 point is about 0.35 mm.

\begin{table}[h]
%\renewcommand{\arraystretch}{1.3}
\caption{Type Size for Camera-Ready Papers}
\label{type_sizes}
\centering
\begin{tabular}{|c|p{10em}|c|c|} 
 \hline
 \multirow{2}{*}{Type size} & \multicolumn{3}{|c|}{Appearance} \\
 \cline{2-4}
 & Regular & Bold & Italic \\
 \hline\hline
 8 & Section titles, references, tables, table names, first letters in table captions, figure captions, footnotes, text subscripts and superscripts &  &  \\ 
 \hline
 9 &  & Abstract,keywords & \\
 \hline
 10 & Authors’ affiliations, main text, equations, first letters in section titles & & Subheading \\
 \hline
 11 & Author's names & & \\
 \hline
 24 & Paper title & & \\ 
 \hline
\end{tabular}
\end{table}

Prepare your camera‑ready paper on the A4 paper size (210 mm x 297 mm). You are not allowed to use US letter-sized paper.

Justify both left and right columns. On the last page of your paper, adjust the lengths of the columns so that they are equal. Use automatic hyphenation and check spelling. Do not add page numbers.

\section{HELPFUL HINTS}

\subsection{Abbreviations and Acronyms}

Define abbreviations and acronyms the first time they are used in the text, even after they have been defined in the abstract. Abbreviations such as IEEE, SI, MKS, CGS, sc, dc, and rms do not have to be defined. Do not use abbreviations in the title or heads unless they are unavoidable.

\subsection{Units}

\begin{itemize}
    \item Use either SI (MKS) or CGS as primary units. (SI units are encouraged.) An exception would be the use of English units as identifiers in trade, such as “3.5-inch disk drive”.
    \item Avoid combining SI and CGS units, such as current in amperes and magnetic field in oersteds. This often leads to confusion because equations do not balance dimensionally. If you must use mixed units, clearly state the units for each quantity that you use in an equation.
    \item Do not mix complete spellings and abbreviations of units: “Wb/m2” or “webers per square meter”, not “webers/m2”. Spell out units when they appear in text: “. . . a few henries”, not “. . . a few H”.
    \item Use a zero before decimal points: “0.25”, not “.25”. Use “cm3”, not “cc”. 
\end{itemize}

\subsection{Figures and Tables}

Place figures and tables at the top and bottom of columns. Avoid placing them in the middle of columns. Large figures and tables may span across both columns. Figure captions should be below the figures; table heads should appear above the tables. Insert figures and tables after they are cited in the text. Use the abbreviation “Fig. 1”, even at the beginning of a sentence.

\begin{figure}
  \label{fig:figure1}
  \centering
  \includegraphics{figure1.jpg}
  \caption{Magnetization as a function of applied field. Note how the caption is centered in the column}
\end{figure}

Use words rather than symbols or abbreviations when writing Figure axis labels to avoid confusing the reader. As an example, write the quantity “Magnetization”, or “Magnetization, M”, not just “M”. If including units in the label, present them within parentheses. Do not label axes only with units. In the example, write “Magnetization (A/m)” or “Magnetization {A[m(1)]}”, not just “A/m”. Do not label axes with a ratio of quantities and units. For example, write “Temperature (K)”, not “Temperature/K”.

\subsection{Equations}

Number equations consecutively. Equation numbers, within parentheses, are to position flush right, as in (1), using a right tab stop. To make your equations more compact, you may use the solidus ( / ), the exp function, or appropriate exponents. Italicize Roman symbols for quantities and variables, but not Greek symbols. Use a long dash rather than a hyphen for a minus sign. Punctuate equations with commas or periods when they are part of a sentence, as in

\begin{equation}
    \alpha + \beta = \chi
\end{equation}

Note that the equation is centered using a center tab stop. Be sure that the symbols in your equation have been defined before or immediately following the equation. Use “(1)”, not “Eq. (1)” or “equation (1)”, except at the beginning of a sentence: “Equation (1) is . . .”

\subsection{Some Common Mistakes}

\begin{itemize}
    \item The word “data” is plural, not singular.
    \item The subscript for the permeability of vacuum $\epsilon_0$, and other common scientific constants, is zero with subscript formatting, not a lowercase letter “o”.
    \item In American English, commas, semi-/colons, periods, question and exclamation marks are located within quotation marks only when a complete thought or name is cited, such as a title or full quotation. When quotation marks are used, instead of a bold or italic typeface, to highlight a word or phrase, punctuation should appear outside of the quotation marks. A parenthetical phrase or statement at the end of a sentence is punctuated outside of the closing parenthesis (like this). (A parenthetical sentence is punctuated within the parentheses.)
    \item A graph within a graph is an “inset”, not an “insert”. The word alternatively is preferred to the word “alternately” (unless you really mean something that alternates).
    \item Do not use the word “essentially” to mean “approximately” or “effectively”.
    \item In your paper title, if the words “that uses” can accurately replace the word “using”, capitalize the “u”; if not, keep using lower-cased.
    \item Be aware of the different meanings of the homophones “affect” and “effect”, “complement” and “compliment”, “discreet” and “discrete”, “principal” and “principle”.
    \item Do not confuse “imply” and “infer”.
    \item The prefix “non” is not a word; it should be joined to the word it modifies, usually without a hyphen.
    \item There is no period after the “et” in the Latin abbreviation “et al.”.
    \item The abbreviation “i.e.” means “that is”, and the abbreviation “e.g.” means “for example”.
\end{itemize}

An excellent style manual for science writers is \cite{young2002technical}.

If your native language is not English, try to get a native English‑speaking colleague, or somebody fluent in English to proofread your paper. Use grammar existent in text editor.

\subsection{References}

The template will number citations consecutively within brackets \cite{eason1955certain}. The sentence punctuation follows the bracket \cite{maxwell1873treatise}. Refer simply to the reference number, as in \cite{jacobs1963fine}—do not use “Ref. \cite{jacobs1963fine}” or “reference \cite{jacobs1963fine}” except at the beginning of a sentence: “Reference \cite{jacobs1963fine} was the first . . .”

Number footnotes separately in superscripts. Place the actual footnote at the bottom of the column in which it was cited. Do not put footnotes in the reference list. Use letters for table footnotes.

Unless there are six authors or more give all authors' names; do not use “et al.”. Papers that have not been published, even if they have been submitted for publication, should be cited as “unpublished” \cite{elissa}. Papers that have been accepted for publication should be cited as “in press” \cite{nicole}. Capitalize only the first word in a paper title, except for proper nouns and element symbols.

For papers published in translation journals, please give the English citation first, followed by the original foreign-language citation \cite{yorozu1987electron}.

\subsection{Other Recommendations}

The Roman numerals are used to number the section headings. Do not number ACKNOWLEDGMENTS and REFERENCES, and begin Subheadings with letters. Use two spaces after periods (full stops). Hyphenate complex modifiers: “zero-field-cooled magnetization.” Avoid dangling participles, such as, “Using (1), the potential was calculated.” Write instead, “The potential was calculated using (1),” or “Using (1), we calculated the potential.”

\section{Conclusion}
Be brief and give most important conclusion from your paper. Do not use equations and figures here.

\section*{acknowledgment}

The preferred spelling of the word “acknowledgment” in America is without an “e” after the “g”. Avoid the stilted expression, “One of us (R. B. G.) thanks . . .”  Instead, try “R. B. G. thanks”. 

\bibliographystyle{IEEEtran}
\bibliography{references}

\end{document}
